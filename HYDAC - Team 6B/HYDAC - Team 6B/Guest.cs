﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HYDAC___Team_6B
{
    public class Guest
    {
        private string guestName;
        private string guestPhone;
        private DateTime arrival;
        private string company;
        private string responsible;
        private string purpose;

        public string GuestName { get; set; }
        public string GuestPhone { get; set; }
        public DateTime Arrival { get; set; }
        public string Company { get; set; }
        public string Responsible { get; set; }
        public string Purpose { get; set; }
        Employee emp;

        public Guest() { }
        public Guest(string guestName)
        {
            this.guestName = guestName;
        }
        public Guest(string guestName, string guestPhone)
        {
            this.guestName = guestName;
            this.guestPhone = guestPhone;
        }
        public Guest(string guestName, string guestPhone, DateTime arrival)
        {
            this.guestName = guestName;
            this.guestPhone = guestPhone;
            this.arrival = arrival;
        }
        public Guest(string guestName, string guestPhone, DateTime arrival, string company)
        {
            this.guestName = guestName;
            this.guestPhone = guestPhone;
            this.arrival = arrival;
            this.company = company;
        }
        public Guest(string guestName, string guestPhone, DateTime arrival,
    string company, string responsible)
        {
            this.guestName = guestName;
            this.guestPhone = guestPhone;
            this.arrival = arrival;
            this.company = company;
            this.responsible = responsible;
        }
        public Guest(string guestName, string guestPhone, DateTime arrival,
    string company, string responsible, string purpose)
        {
            this.guestName = guestName;
            this.guestPhone = guestPhone;
            this.arrival = arrival;
            this.company = company;
            this.responsible = responsible;
            this.purpose = purpose;
        }
        public List<Guest> Guests;

        public void AddToList(Guest guest)
        {
            Guests.Add(guest);
        }
        private void CallResponsible(string employeeName, string phoneNumber)
        {
            emp.EmployeeName = employeeName;
            emp.PhoneNumber = phoneNumber;
            //skal kalde på en employee der er Responsible
        }
        public override string ToString()
        {
            return $"{guestName};{guestPhone};{purpose};{arrival};{responsible};{company}";
        }
    }
}
