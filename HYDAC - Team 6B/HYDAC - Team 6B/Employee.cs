﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HYDAC___Team_6B
{
    //public enum TakeCare
    //{ Grøn, Gul, Rød }

    public class Employee
    {
        public string EmployeeName { get; set; }
        public string UserLogin { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsCheckedIn { get; set; }
        public TakeCare takeCare { get;  set; }

        public void setTakeCare(TakeCare value)
        {
            takeCare = value;
        }

        public TakeCare getTakeCare()
        {
            return takeCare;
        }

        //use: guestname, responsible
        //Guest g;
        //public void InviteGuest(string guestName, string guestLogin)
        //{ sad
        //  g.GuestName = guestName;
        //  g.GuestPhone = guestLogin;
        //}
        public override string ToString()
        {
            return $"{EmployeeName};{UserLogin};{PhoneNumber}";
        }
    }
}