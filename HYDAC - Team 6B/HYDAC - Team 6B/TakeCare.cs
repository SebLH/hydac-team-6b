﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HYDAC___Team_6B
{
    public enum TakeCare { Grøn, Gul, Rød  }

    public class TakeCareDataHandler
    {
        private string dataFileName;
        public string DataFileName
        {
            get { return dataFileName; }
        }
        public TakeCareDataHandler(string dataFileName) // TakeCareDataHandler DataObject = new TakeCareDataHandler("asd");
        {
            this.dataFileName = dataFileName;
        }
        public void SaveTakeCare(Employee employee)
        {
            StreamWriter writer = new StreamWriter(dataFileName);
            writer.WriteLine($"{employee.EmployeeName} : {DateTime.Now} : {employee.getTakeCare()}");
            writer.Close();
        }

        //public List<Employee.TakeCare> LoadTakeCare()
        //{
        //    string[] TakeCareStringList = File.ReadAllLines(dataFileName);
        //    List<Takecare> takeCares = new TakeCare[TakeCareStringList.Length];
        //}
    }
}