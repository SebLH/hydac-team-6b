﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HYDAC___Team_6B
{
    public class DataHandler //Taget direkte fra LoadPersons - Ex12
    {
        private string roomFileName;
        private string employeeFileName;
        public string EmployeeFileName
        {
            get { return employeeFileName; }
        }
        public string RoomFileName
        {
            get { return roomFileName; }
        }
        public DataHandler (string roomFileName, string employeeFileName)
        {
            this.roomFileName = roomFileName;
            this.employeeFileName = employeeFileName;
        }
        public void SaveRoom(Room room)
        {
            StreamWriter sw = new StreamWriter(RoomFileName);
            sw.WriteLine(room.CreateRoom());
            sw.Close();
        }
        public void SaveRooms(Room[] rooms)
        {
            StreamWriter sw = new StreamWriter(RoomFileName);
            foreach (var room in rooms)
            {
                sw.WriteLine(room.CreateRoom());
            }
            sw.Close();
        }
        public Room[] LoadRooms() 
        {
            Room[] rooms = new Room[roomFileName.Length];
            string lines;
            int count = 0;
            StreamReader sr = new StreamReader(roomFileName);
            while ((lines = sr.ReadLine()) != null)
            {
                string[] roomAttributes = lines.Split(" | ");
                rooms[count] = new Room(roomAttributes[0],
                int.Parse(roomAttributes[1]),
                int.Parse(roomAttributes[2]));
            }
            sr.Close();
            return rooms;
        }
    }
}
