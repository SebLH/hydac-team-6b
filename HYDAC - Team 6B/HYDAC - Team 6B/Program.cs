﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MenuTree;

namespace HYDAC___Team_6B
{
    class Program
    {
        static void Main(string[] args)
        {

            // Menu Node
                                                               


            //Menu mainMenu = new Menu($"Antal personer checket ind: { counter }\n\n");
            DateTime dt = DateTime.Now;

            Menu mainMenu = new Menu();
            Menu menu1 = new Menu();
            Menu menu13 = new Menu();
            Menu menu101 = new Menu();
            Menu menu2 = new Menu();
            bool runMenu = true;

            Guest g = new Guest();
            g.Guests = new List<Guest>();

            mainMenu.AddMenuItem("1. Medarbejder");
            mainMenu.AddMenuItem("2. Gæst");
            mainMenu.AddMenuItem("0. Luk konsollen");
            mainMenu.Show();

            menu1.AddMenuItem("\n1. Check ind");
            menu1.AddMenuItem("2. Check ud");
            menu1.AddMenuItem("3. Vælg humør");
            menu1.AddMenuItem("4. Inviter gæst");
            menu1.AddMenuItem("5. Gå tilbage til start");

            menu13.AddMenuItem("Grøn");
            menu13.AddMenuItem("Gul");
            menu13.AddMenuItem("Rød");

            menu101.AddMenuItem("\n1. Print employees");
            menu101.AddMenuItem("2. Print rooms");
            menu101.AddMenuItem("3. Opret employee (Admin)");

            menu2.AddMenuItem("\n1. Check ind");
            menu2.AddMenuItem("2. Check ud");

            while (runMenu)
            {
                
                DataHandler handler = new DataHandler("Rooms.txt", "Employees.txt"); //Definerer vores tekstfiler vi vil oprette og bruge.
                // Vi opretter alle rummene her, for at vise at vores saverooms fungerer
                Room[] rooms = new Room[]
                {
                new Room("Lille Stue", 4, 14),
                new Room("Stilling Kantine", 1, 1),
                new Room("Stilling Stueetage", 1, 1),
                new Room("The Aquarium", 1, 1),
                new Room("The Bride East", 1, 1),
                new Room("The Bride West", 1, 1),
                new Room("The Canteen North", 1, 1),
                new Room("The Station - Playform 9¾", 1, 1),
                new Room("The Station -  The Coffee Shop", 1, 1),
                new Room("The Station - The Library", 1, 1),
                new Room("The Training Center", 1, 1),
                new Room("LokaleLille", 1, 1),
                new Room("LokaleService", 1, 1),
                new Room("LokaleStor", 1, 1)
                };
                handler.SaveRooms(rooms);

                // Nedenfor er kun for testing!!
                // bool HovedMenu = true;
                // Vi opretter Ansatte
                List<Employee> emps = new List<Employee>();
                List<string> lines = File.ReadAllLines("Employee.txt").ToList();
                Employee employee = new Employee();



                mainMenu.Show();

                int choice = mainMenu.SelectMenuItem();
            foreach (var line in lines)
            {
                string[] entries = line.Split(" | ");

                Employee newEmployee = new Employee();

                newEmployee.EmployeeName = entries[0];
                newEmployee.UserLogin = entries[1];
                newEmployee.PhoneNumber = entries[2];
                emps.Add(newEmployee);
            }

            if (choice == 1)
            {
                Console.Clear();
                Console.WriteLine("Indtast dit login"); // Login - Vi læser alle linjer og indsætter matchende navn og nummer, hvor det sidste der mangler er deres login.
                string userInput = Console.ReadLine();
                if (userInput.Length == 4 && File.ReadAllText("Employee.txt").Contains(employee.EmployeeName + " | " + userInput + " | " + employee.PhoneNumber))
                {
                    menu1.Show();
                    switch (Console.ReadLine())
                    {
                        case "1":
                            mainMenu.counter++;
                            continue;
                        case "2":
                            mainMenu.counter--;
                            continue;
                        case "3":
                            menu13.Show();
                            switch (Console.ReadLine())
                            {
                                case "1":

                                    continue;


                                    default:
                                        Console.WriteLine("Tryk på en vilkårlig tast for at gå tilbage");
                                        Console.ReadKey();
                                        continue;
                            }

                        case "4":
                            Console.WriteLine("Indtast gæstens navn: ");
                            g.GuestName = Console.ReadLine();
                            Console.WriteLine("Indtast gæstens tlf nr: ");
                            g.GuestPhone = Console.ReadLine();

                            Console.WriteLine("Skal gæsten inviteres til et møde?");
                            Console.WriteLine("1. Ja");
                            Console.WriteLine("2. Nej");
                            switch (Console.ReadLine())
                            {
                                case "1":
                                    g.Purpose = "Meeting";
                                    break;
                                case "2":
                                    g.Purpose = "Work";
                                    break;
                                default:
                                    g.Purpose = "Unknown";
                                    break;
                            }
                            Console.WriteLine("Hvornår skal gæsten ankomme?");
                            Console.WriteLine("Format: [YYYY,MM,DD,HH:mm]" +
                                              "\nEksempel: 2021,12,30,13:30");
                            try
                            {
                                g.Arrival = Convert.ToDateTime(Console.ReadLine());
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Invalid dato, går tilbage til start...");
                                mainMenu.counter--;
                                System.Threading.Thread.Sleep(1000);
                                continue;
                                throw;
                            }
                            Console.WriteLine("Hvilken virksomhed er de fra?");
                            g.Company = Console.ReadLine();
                            g.AddToList(g); //guestName, guestLogin, g.Purpose, g.Arrival, userInput, g.Company);
                            g.Responsible = userInput;

                            //Se om det virker
                            foreach (var guest in g.Guests)
                            {
                                Console.WriteLine($"{g.GuestName} | {g.GuestPhone} | {g.Purpose} | {g.Arrival} | {g.Responsible} | {g.Company}");
                            }

                            Console.ReadLine();
                            continue;
                        case "5":
                            continue;
                            default:
                                Console.WriteLine("Tryk på en vilkårlig tast for at gå tilbage");
                                Console.ReadKey();
                                continue;
                    }
                }
                    else if (userInput == "Admin")
                    {
                        menu101.Show();
                        switch (Console.ReadLine())
                        {
                            case "1":
                                foreach (var emp in emps)
                                {
                                    Console.WriteLine($"{emp.EmployeeName} | {emp.UserLogin} | {emp.PhoneNumber}");
                                }
                                Console.WriteLine("Tryk på en vilkårlig tast for at gå tilbage");
                                Console.ReadKey();
                                continue;
                            case "2":
                                Console.WriteLine("\nIndtast først medarbejders navn:");
                                string tempEmployeeName = Console.ReadLine();
                                Console.WriteLine("Indtast medarbejders login (SKAL være 4 karakterer):");
                                string tempUserLogin = Console.ReadLine();
                                Console.WriteLine("Indtast medarbejders tlf nr:");
                                string tempPhoneNumber = Console.ReadLine();
                                emps.Add(new Employee { EmployeeName = tempEmployeeName, UserLogin = tempUserLogin, PhoneNumber = tempPhoneNumber });
                                List<string> output = new List<string>();
                                foreach (var emp in emps)
                                {
                                    output.Add($"{emp.EmployeeName} | {emp.UserLogin} | {emp.PhoneNumber}");
                                }
                                File.WriteAllLines("Employee.txt", output);
                                Console.WriteLine("Tryk på en vilkårlig tast for at gå tilbage");
                                Console.ReadKey();
                                continue;
                            case "3":
                                foreach (var room in rooms)
                                {
                                    Console.WriteLine(room.CreateRoom());
                                }
                                Console.WriteLine("Tryk på en vilkårlig tast for at gå tilbage");
                                Console.ReadKey();
                                continue;
                                
                            default:
                                Console.WriteLine("Tryk på en vilkårlig tast for at gå tilbage");
                                Console.ReadKey();
                                continue;
                        }
                    }
                else
                {
                    Console.WriteLine("Dit login er forkert, tryk på en knap for at gå tilbage til start.");
                    Console.ReadLine();
                    continue;
                }
            }
            if (choice == 2)
            {
                Console.WriteLine("Indtast dit tlf nr:");
                string guestInput = Console.ReadLine();
                foreach (var guest in g.Guests)
                {
                    Console.WriteLine(guest);
                    if (guest.GuestPhone == guestInput)
                    {
                        menu2.Show();
                        switch (Console.ReadLine())
                        {
                            case "1":
                                mainMenu.counter++;
                                continue;
                            case "2":
                                mainMenu.counter--;
                                continue;
                                default:
                                    Console.WriteLine("Tryk på en vilkårlig tast for at gå tilbage");
                                    Console.ReadKey();
                                    continue;
                        }
                    }
                }
            }
            else
            {
                runMenu = false;
                Console.WriteLine("Konsollen lukker...");
                System.Threading.Thread.Sleep(1500);
            }
        }
    }
    }
}
