﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HYDAC___Team_6B
{
    public class Meeting
    {
        private DateTime time;
        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }

        private string agenda;
        public string Agenda
        {
            get { return agenda; }
            set { agenda = value; }
        }

        public List<Guest> GuestAttendees;
        public List<Employee> EmployeeAttend;

        public Meeting(DateTime time, string agenda)
        {
            Time = time;
            Agenda = agenda;
            GuestAttendees = new List<Guest>();
            EmployeeAttend = new List<Employee>();
        }

        public void AddToList(Employee employeeAttend)
        {
            EmployeeAttend.Add(employeeAttend);
        }

        public void AddToList(Guest guestAttendees)
        {
            GuestAttendees.Add(guestAttendees);
        }

        public List<string> Attendees = new List<string>();
        public List<string> GetAttendees()
        {
            Attendees.Add("");
            Attendees.Add("Employees:");
            foreach (var employee in EmployeeAttend)
            {
                Attendees.Add(employee.EmployeeName);
            }
            Attendees.Add("");
            Attendees.Add("Guests:");
            foreach (var guest in GuestAttendees)
            {
                Attendees.Add(guest.GuestName);
            }

            return Attendees;
        }

        // Attendees = Attendees.GetAttendees();
        public string ReturnMeeting()
        {
            return ($" {Time} \n{GetAttendees()}.");
        }
    }
}
