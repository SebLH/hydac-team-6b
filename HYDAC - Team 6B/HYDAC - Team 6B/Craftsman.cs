﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HYDAC___Team_6B
{
    public class Craftsman
    {
        private string craftName;
        private string craftPhone;
        private DateTime arrival;
        private string userLogin;
        private string company;
        private string responsible;
        private string purpose;
        private bool security;
        private string employeeName;
        private string phoneNumber;

        public string CraftName
        {
            get { return craftName; }
            set { craftName = value; }
        }
        public string CrafPhone
        {
            get { return craftPhone; }
            set { craftPhone = value; }
        }
        public DateTime Arrival
        {
            set { arrival = value; }
        }
        public string UserLogin
        {
            get { return userLogin; }
            set { userLogin = value; }
        }
        public string Company
        {
            get { return company; }
            set { company = value; }
        }
        public string Responsible
        {
            get { return responsible; }
            set { responsible = value; }
        }
        public string Purpose
        {
            get { return purpose; }
            set { purpose = value; }
        }
        public bool Security
        {
            get { return security; }
            set { security = value; }
        }
        public string EmployeeName
        {
            set { employeeName = value; }
            get { return EmployeeName; }
        }
        public string PhoneNumber
        {
            set { phoneNumber = value; }
            get { return phoneNumber; }
        }

        public Craftsman(string craftsName, string craftPhone, DateTime arrival, string company)
        {
            this.craftName = craftsName;
            this.craftPhone = craftPhone;
            this.arrival = arrival;
            this.company = company;
         }

        private void CallResponsible(string employeeName, string phoneNumber)
        {
            EmployeeName  = employeeName;
            PhoneNumber = phoneNumber;
        }
    }
}

        


