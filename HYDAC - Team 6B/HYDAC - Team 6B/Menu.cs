﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HYDAC___Team_6B
{
    public class Menu
    {
        public string Title;
        private int itemCount = 0;
        private MenuItem[] menuItems = new MenuItem[1000];
        public int counter = 0;

        public Menu(string title)
        {
            this.Title = title;
        }

        public Menu()
        {

        }

        internal void PrintMenu()
        {
            Console.WriteLine("Antal personer checket ind: {0}", counter);
        }

        public void Show()
        {
            Console.Clear();
            PrintMenu();
            Console.WriteLine(Title);
            for (int i = 0; i < menuItems.Length; i++)
            {
                if (menuItems[i] != null)
                {
                    Console.WriteLine(menuItems[i].Title);
                }
                else
                {
                    break;
                }
            }
        }

        public void AddMenuItem(string menuTitle)
        {
            menuItems[itemCount] = new MenuItem(menuTitle);
            itemCount++;
        }

        public void RemoveMenuItem()
        {
            Array.Clear(menuItems, 0, 6);
        }

        public int SelectMenuItem()
        {
            if (int.TryParse(Console.ReadLine(), out int result))
            {
                return result;
            }
            else
            {
                return -1;
            }
        }
    }
}
