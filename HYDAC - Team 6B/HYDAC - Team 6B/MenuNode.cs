﻿using System;
using System.Collections.Generic;

namespace MenuTree
{
    public class MenuNode<T>
    {
        public T Data { get; set; }
        public MenuNode<T> Parent { get; set; }
        public List<MenuNode<T>> Children { get; set; }
        public bool IsRoot { get; set; }
        public bool IsLive { get; set; }

        public MenuNode(T data, bool isRoot)
        {
            IsLive = false;
            IsRoot = isRoot;
            Data = data;
            Children = new List<MenuNode<T>>();
        }
        public MenuNode(T data) : this(data, false) { }

        public void Show()
        {
            this.IsLive = true;
            Console.WriteLine($"{Data}");
            bool first = true;
            foreach (MenuNode<T> child in Children)
            {
                if (first)
                {
                    first = false; // skips first iteration.
                }
                else
                    Console.WriteLine($"{Children.IndexOf(child)}: {child.Data}");
            }

            /* Handles userinput and moves to the desired node.*/
            string userInputString = Console.ReadLine();
            bool inpuIsNumber = int.TryParse(userInputString, out int userInputInt);

            if ( inpuIsNumber && userInputInt.Equals(0))    
            {
                this.IsLive = false;
                Parent.Show();
            }
            else if (inpuIsNumber && userInputInt <= Children.Count)
            {
                this.IsLive = false;
                Children[userInputInt].Show();
            }
            else
            {
                this.IsLive = false;
                Show();
            }
        }

        //public void Hide()
        //{
        //    Console.Clear();
        //    this.IsLive = false;
        //}

        public MenuNode<T> AddChild(T Data)
        {
            MenuNode<T> child = new MenuNode<T>(Data) { Parent = this };
            Children.Add(child);
            return child;
        }

        public int GetHeight()
        {
            MenuNode<T> parent = Parent;
            if (parent.IsRoot)
            {
                return 0;
            }
            else
            {
                return parent.GetHeight() + 1;
            }
        }
    }
}


//MenuNode<string> Root = new MenuNode<string>("Root", true);
//MenuNode<string> node0 = Root.AddChild("back");
//MenuNode<string> node1 = Root.AddChild("node 1");
//MenuNode<string> node2 = Root.AddChild("node 2");
//MenuNode<string> node20 = node2.AddChild("back");
//MenuNode<string> node21 = node2.AddChild("node 2 1");
//MenuNode<string> node210 = node20.AddChild("back");
//MenuNode<string> node211 = node20.AddChild("node 2 1 1");
//MenuNode<string> node22 = node2.AddChild("node 2 2");

//Root.Show();
//bool rootPrinted = false;
//MenuNode<string> Root = new MenuNode<string>($"HYDAC - Personer checket ind: {mainMenu.counter}", true);

//MenuNode<string> node0 = Root.AddChild("Tilbage");
//// Nyt menu item
//MenuNode<string> node1 = Root.AddChild("Medarbejder");
//// Vi går ind
//MenuNode<string> node10 = node1.AddChild("Tilbage");
//MenuNode<string> node11 = node1.AddChild("Check ind");
//// Vi går ind
//MenuNode<string> node110 = node11.AddChild("Tilbage");
//MenuNode<string> node111 = node11.AddChild("Vælg humør");
//// Vi går ind igen
//MenuNode<string> node1110 = node111.AddChild("Tilbage");
//MenuNode<string> node1111 = node111.AddChild("Grøn");
//MenuNode<string> node1112 = node111.AddChild("Gul");
//MenuNode<string> node1113 = node111.AddChild("Rød");
//// Hurtig funktion - Vi går ikke dybere
//MenuNode<string> node12 = node1.AddChild("Check ud");
//// Hurtig funktion - Vi går ikke dybere
//MenuNode<string> node13 = node1.AddChild("Inviter gæst");
//// Nyt menu item
//MenuNode<string> node2 = Root.AddChild("Gæst");
//// Vi går ind
//MenuNode<string> node20 = node2.AddChild("back");
//MenuNode<string> node21 = node2.AddChild("Check ind");
//MenuNode<string> node22 = node2.AddChild("Check Ud");
//// Nyt menu item
//MenuNode<string> node3 = Root.AddChild("TakeCare tavle");


//while (node1.IsLive) // node1.Hide();
//{
//        //Root.Hide();
//        // node1.Hide();
//        Console.WriteLine("Indtast dit login"); 
//        string userInput = Console.ReadLine();
//        if (userInput.Length == 4 && File.ReadAllText("Employee.txt").Contains(employee.EmployeeName + " | " + userInput + " | " + employee.PhoneNumber))
//        { // Login - Vi læser alle linjer og indsætter matchende navn og nummer, hvor det sidste der mangler er deres login.
//            //MenuNode<string> node10 = node1.AddChild("Tilbage");
//            //MenuNode<string> node11 = node1.AddChild("Check ind");
//        while (node11.IsLive) // node111.Hide();
//        {
//            //node1.Hide();
//            mainMenu.counter++;
//            Console.WriteLine("Du er checket ind");
//            Console.WriteLine("\nTryk på en vilkårlig tast for at gå tilbage");
//            Console.ReadKey();
//            node1.Show();
//        }
//        //MenuNode<string> node110 = node11.AddChild("Tilbage");
//        //MenuNode<string> node111 = node11.AddChild("Vælg humør");
//        while (node111.IsLive) // node111.Hide();
//        {
//            //MenuNode<string> node1110 = node111.AddChild("Tilbage");
//            //MenuNode<string> node1111 = node111.AddChild("Grøn");
//            TakeCare Green = TakeCare.Grøn;
//            node1.Show();
//            //MenuNode<string> node1112 = node111.AddChild("Gul");
//            TakeCare Yellow = TakeCare.Gul;
//            node1.Show();
//            //MenuNode<string> node1113 = node111.AddChild("Rød");
//            TakeCare Red = TakeCare.Rød;
//            node1.Show();
//        }

//        //MenuNode<string> node12 = node1.AddChild("Check ud");
//        mainMenu.counter--;
//            Console.WriteLine("Du er checket ud");
//            Console.WriteLine("\nTryk på en vilkårlig tast for at gå tilbage");
//            Console.ReadKey();
//            // node1.Show();

//        //MenuNode<string> node13 = node1.AddChild("Inviter gæst");
//        Console.WriteLine("Indtast gæstens navn: ");
//            g.GuestName = Console.ReadLine();
//            Console.WriteLine("Indtast gæstens tlf nr: ");
//            g.GuestPhone = Console.ReadLine();
//            Console.WriteLine("Skal gæsten inviteres til et møde?");
//            Console.WriteLine("1. Ja");
//            Console.WriteLine("2. Nej");
//            switch (Console.ReadLine())
//            {
//                case "1":
//                    g.Purpose = "Meeting";
//                    break;
//                case "2":
//                    g.Purpose = "Work";
//                    break;
//                default:
//                    g.Purpose = "Unknown";
//                    break;
//            }
//            Console.WriteLine("Hvornår skal gæsten ankomme?");
//            Console.WriteLine("Format: [YYYY,MM,DD,HH:mm]" +
//                                "\nEksempel: 2021,12,30,13:30");
//            try
//            {
//                g.Arrival = Convert.ToDateTime(Console.ReadLine());
//            }
//            catch (Exception)
//            {
//                Console.WriteLine("Invalid dato, tryk på en vilkårlig tast for at gå tilbage...");
//                Console.ReadKey();
//                node13.Show();
//                throw;
//            }
//            Console.WriteLine("Hvilken virksomhed er de fra?");
//            g.Company = Console.ReadLine();
//            g.AddToList(g); //guestName, guestLogin, g.Purpose, g.Arrival, userInput, g.Company);
//            g.Responsible = userInput;

//            //Se om det virker
//            foreach (var guest in g.Guests)
//            {
//                Console.WriteLine($"{g.GuestName} | {g.GuestPhone} | {g.Purpose} | {g.Arrival} | {g.Responsible} | {g.Company}");
//            }
//            Console.WriteLine("Tryk på en vilkårlig tast for at gå tilbage");
//            Console.ReadKey();
//            // node1.Show();
//        }
//        //Admin ting
//        else if (userInput == "Admin")
//        {
//        //MenuNode<string> node10 = node1.AddChild("Tilbage");
//        //MenuNode<string> node11 = node1.AddChild("Print employees");
//        foreach (var emp in emps)
//                {
//                    Console.WriteLine($"{emp.EmployeeName} | {emp.UserLogin} | {emp.PhoneNumber}");
//                }
//                Console.WriteLine("Tryk på en vilkårlig tast for at gå tilbage");
//                Console.ReadKey();
//                node1.Show();

//        //MenuNode<string> node12 = node1.AddChild("Opret ny employee");
//        Console.WriteLine("\nIndtast først medarbejders navn:");
//                string tempEmployeeName = Console.ReadLine();
//                Console.WriteLine("Indtast medarbejders login (SKAL være 4 karakterer):");
//                string tempUserLogin = Console.ReadLine();
//                Console.WriteLine("Indtast medarbejders tlf nr:");
//                string tempPhoneNumber = Console.ReadLine();
//                emps.Add(new Employee { EmployeeName = tempEmployeeName, UserLogin = tempUserLogin, PhoneNumber = tempPhoneNumber });
//                List<string> output = new List<string>();
//                foreach (var emp in emps)
//                {
//                    output.Add($"{emp.EmployeeName} | {emp.UserLogin} | {emp.PhoneNumber}");
//                }
//                File.WriteAllLines("Employee.txt", output);
//                Console.WriteLine("Tryk på en vilkårlig tast for at gå tilbage");
//                Console.ReadKey();
//                node1.Show();

//        //MenuNode<string> node13 = node1.AddChild("Print rooms");
//        foreach (var room in rooms)
//                {
//                    Console.WriteLine(room.CreateRoom());
//                }
//                Console.WriteLine("Tryk på en vilkårlig tast for at gå tilbage");
//                Console.ReadKey();
//                node1.Show();
//        }

//        else
//        {
//            Console.WriteLine("Dit login er forkert, tryk på en knap for at gå tilbage til start.");
//            Console.ReadLine();
//            Root.Show();
//        }
//}
////MenuNode<string> node2 = Root.AddChild("Gæst");
//while (node2.IsLive)
//{
//    Console.WriteLine("Indtast dit tlf nr:");
//    string guestInput = Console.ReadLine();
//    foreach (var guest in g.Guests)
//    {
//        Console.WriteLine(guest);
//        if (guest.GuestPhone == guestInput)
//        {
//            //MenuNode<string> node20 = node2.AddChild("back");
//            //MenuNode<string> node21 = node2.AddChild("Check ind");
//            mainMenu.counter++;
//            Console.WriteLine("Du er checket ind");
//            System.Threading.Thread.Sleep(750);
//            Root.Parent.Show();
//            //MenuNode<string> node22 = node2.AddChild("Check Ud");
//            mainMenu.counter--;
//            Console.WriteLine("Du er checket ud");
//            System.Threading.Thread.Sleep(750);
//            Root.Parent.Show();
//            break;
//        }
//    }
//}
//        //MenuNode<string> node3 = Root.AddChild("TakeCare tavle");
//rootPrinted = true;
//Root.Show();